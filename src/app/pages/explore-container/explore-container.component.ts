import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { ServicioService } from "../../services/servicio.service";
import { DatosCuriosos } from "../../interfaces/interfaces";

@Component({
  selector: "app-explore-container",
  templateUrl: "./explore-container.component.html",
  styleUrls: ["./explore-container.component.scss"]
})
export class ExploreContainerComponent implements OnInit {
  DatosCuriosos: DatosCuriosos;
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  constructor(
    private servicioService: ServicioService,
    private router: Router
  ) {}

  ngOnInit() {
    this.servicioService.getDatoCurioso().subscribe((resp: any) => {
      this.DatosCuriosos = resp;

      console.log(this.DatosCuriosos);
      return this.DatosCuriosos;
    });
  }
}
