import { Component, OnInit, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { Categoria } from "../../interfaces/interfaces";
import { ServicioService } from "../../services/servicio.service";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.page.html",
  styleUrls: ["./detail.page.scss"]
})
export class DetailPage implements OnInit {
  @Input() Categorias;

  // categoria: Categoria = {};

  constructor(
    private modalCtrl: ModalController,
    private servicioService: ServicioService
  ) {}

  ngOnInit() {
    this.servicioService.getDatoCurioso().subscribe(resp => {
      console.log(resp);
      // this.categoria = resp;

      // return this.categoria;
    });
  }
}
