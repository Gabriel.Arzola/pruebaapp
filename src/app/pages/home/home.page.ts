import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ModalController, NavController } from "@ionic/angular";
import { ServicioService } from "../../services/servicio.service";
import { DatosCuriosos, Categorias } from "../../interfaces/interfaces";
import { DetailPage } from "../detail/detail.page";
import { DetalleComponent } from "../../components/detalle/detalle.component";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"]
})
export class HomePage implements OnInit {
  DatosCuriosos: DatosCuriosos;
  Categorias = [];
  slideOpts = {
    initialSlide: 1.3,
    freeMode: true,
    speed: 400,
    slidesPerView: 2.3,
    spaceBetween: 0
  };

  constructor(
    private servicioService: ServicioService,
    private router: Router,
    private navCtrl: NavController,
    private modalCtrl: ModalController
  ) {}

  ngOnInit() {
    this.servicioService.getDatoCurioso().subscribe((resp: any) => {
      this.DatosCuriosos = resp;

      console.log(this.DatosCuriosos);
      return this.DatosCuriosos;
    });

    this.servicioService.getCategoria().subscribe((resp: any) => {
      this.Categorias = resp;

      console.log(this.Categorias);
      return this.Categorias;
    });
  }

  async verDetalle(id: string, imagen: string, nombre: string) {
    const modal = await this.modalCtrl.create({
      component: DetalleComponent,
      componentProps: {
        id,
        imagen,
        nombre
      }
    });

    modal.present();
  }
}
