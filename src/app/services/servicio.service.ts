import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DatosCuriosos } from "../interfaces/interfaces";
import { environment } from "../../environments/environment";

const URL = environment.url;

@Injectable({
  providedIn: "root"
})
export class ServicioService {
  constructor(private http: HttpClient) {}

  getDatoCurioso() {
    return this.http.get("https://api.chucknorris.io/jokes/random");
    // return query;
  }
  getCategoria() {
    return this.http.get("../../assets/json/categorias.json");
    // return query;
  }

  getBusquedaPorCategoria(cat) {
    return this.http.get(
      `https://api.chucknorris.io/jokes/random?category=${cat}`
    );
    // return query;
  }
}
