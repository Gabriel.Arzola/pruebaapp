import { Component, OnInit, Input } from "@angular/core";

import { Categoria, DatosCuriosos } from "../../interfaces/interfaces";
import { ServicioService } from "../../services/servicio.service";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-detalle",
  templateUrl: "./detalle.component.html",
  styleUrls: ["./detalle.component.scss"]
})
export class DetalleComponent implements OnInit {
  DatosCuriosos: DatosCuriosos;
  @Input() id;
  @Input() imagen;
  @Input() nombre;
  category: Categoria[] = [];

  constructor(
    private modalCtrl: ModalController,
    private servicioService: ServicioService
  ) {}

  ngOnInit() {
    this.servicioService.getDatoCurioso().subscribe((resp: any) => {
      this.DatosCuriosos = resp;

      console.log(this.DatosCuriosos);
      return this.DatosCuriosos;
    });

    console.log(this.id);
    console.log(this.imagen);
  }

  regresar() {
    this.modalCtrl.dismiss();
  }
}
