export interface DatosCuriosos {
  categories: number;
  created_at: boolean;
  icon_url: number;
  id: string;
  updated_at: string;
  url: string;
  value: string;
}

export interface Categoria {
  categories: [];
  created_at: string;
  icon_url: string;
  id: string;
  updated_at: string;
  url: string;
  value: string;
}

export interface Categorias {
  id: number;
  nombre: string;
  imagen: string;
  updated_at: string;
  url: string;
  value: string;
}
